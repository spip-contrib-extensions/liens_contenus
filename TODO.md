Plugin Liens entre contenus
===========================

Roadmap
-------
Ce chapitre donne la liste de ce qui est envisagé pour les prochains travaux

### Version 0.30.0 :

* Compatibilité SPIP 3.0.X [ En cours ]

### Version 0.30.1 :

* Mettre à jour le systeme d`installation pour gérer les upgrades depuis précédentes versions	
* Traiter les  cas de liens / images et documents

### Version 0.31.0 :

* Compatibilité SPIP 3.1.X [ En cours ]

### A planifier (ou pas)

* Ajouter un contrôle pour les articles virtuels (pour la disparition de l`article d`origne)
* Etendre le contrôle de dépublication aux actions dans les listes
* Proposer une option de configuration pour ne pas mettre dr`alertes
* Proposer une option de configuration pour interdire la suppression d`objets liés depuis des objets en ligne ou juste alerter
* Référencement des liens externes ? (rapprochement avec le plugin {checklink} de Cédric ?)
* Page de synthèse des soucis relevés ?
* Offrir la possibilité à l`activation de scanner tous les contenus pour remplir les tables ? Ou dans une page d`administration ?


Checklists
----------
Ce chapitre donne les liste des choses à vérifier lorsqu`on effectue une opération particulière

### Pour un commit

### Pour un branchement ou d`une mise à jour /trunk -> /branches/A.B.X

### Pour un passage en stable et copie de branches/A.B.X -> tags -> A.B.Y

