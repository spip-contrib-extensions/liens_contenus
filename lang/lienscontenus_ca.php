<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/lienscontenus?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => 'Alerta, aquest contingut és públic, però conté enllaços cap a continguts que no ho són!',
	'aucun_objets_avec_lien_depuis_courant' => 'Aquest contingut no conté cap enllaç cap a un altre contingut. ',
	'aucun_objets_avec_lien_vers_courant' => 'Cap altre contingut conté un enllaç cap aquest. ',

	// C
	'confirmation_depublication' => 'Atenció, un contingut públic apunta cap aquí, i es pot modificar si el despubliqueu! nnVoleu veritablement canviar l’estat?', # MODIF
	'confirmation_publication' => 'Atenció, un contingut cap al que apunta aquest no és públic!nnVoleu realment canviar l’estat?', # MODIF
	'confirmation_suppression' => 'Atenció, un contingut públic apunta cap aquest, i serà modificat si el suprimiu!nnVoleu realment suprimir-lo?', # MODIF

	// I
	'information_element_contenu' => 'Alerta, un altre contingut apunta cap aquest!', # MODIF

	// L
	'legende_liens_faux_objets' => 'Els enllaços en vermell indiquen continguts relacionats que no existeixen.',

	// O
	'objets_avec_liens_depuis_courant' => 'Aquest contingut conté enllaços cap aquests:', # MODIF
	'objets_avec_liens_vers_courant' => 'Aquests continguts contenen enllaços cap aquest:', # MODIF

	// S
	'statut_poubelle' => 'A la brossa',
	'statut_prepa' => 'En preparació',
	'statut_prop' => 'Proposat',
	'statut_publie' => 'Publicat',
	'statut_refuse' => 'Refusat'
);
