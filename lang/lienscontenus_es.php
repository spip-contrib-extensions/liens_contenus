<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/lienscontenus?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => 'Advertencia, este contenido se publica, pero que contiene enlaces que no están correctos!', # RELIRE
	'alerte_publie_contenant_ko' => 'Advertencia, este contenido se publica, pero contiene enlaces a contenido inexistente!', # RELIRE
	'aucun_objets_avec_lien_depuis_courant' => 'Este contenido no contiene enlaces a otros contenidos.',

	// I
	'inexistant' => 'inexistente (@id_objet@)',
	'information_element_contenu' => 'Atención, enlaces internos apuntando a este contenido!',

	// L
	'legende_liens_faux_objets' => 'Los enlaces en rojo y tachado indican que el contenido relacionado no existe.',
	'liens_entre_contenus' => 'Vínculos entre contenidos',

	// S
	'statut_poubelle' => 'A la basura',
	'statut_prepa' => 'En preparación',
	'statut_prop' => 'Propuesta',
	'statut_publie' => 'Publicado',

	// T
	'type_article_inexistant' => 'Artículo inexistente (@id_objet@)',
	'type_auteur_inexistant' => 'Autor inexistente (@id_objet@)',
	'type_breve_inexistant' => 'Comentario inexistente (@id_objet@)', # RELIRE
	'type_document_inexistant' => 'Documento inexistente (@id_objet@)',
	'type_forum_inexistant' => 'Mensaje inexistente (@id_objet@)',
	'type_modele' => 'Modelo "@id_objet@"',
	'type_modele_inexistant' => 'Modelo inexistente (@id_objet@)',
	'type_rubrique_inexistant' => 'Sección inexistente (@id_objet@)',
	'type_syndic_inexistant' => 'Sitio inexistente (@id_objet@)'
);
