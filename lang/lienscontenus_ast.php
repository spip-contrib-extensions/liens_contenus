<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/lienscontenus?lang_cible=ast
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'alerte_publie_contenant' => '¡Atención, esti conteníu ta espublizáu, pero contien enllaces a conteníos que nun tan!',
	'aucun_objets_avec_lien_depuis_courant' => 'Esti conteníu nun contién dengún enllaz a otru conteníu.',
	'aucun_objets_avec_lien_vers_courant' => 'Dengún otru conteníu contién un enllaz a esti.',

	// C
	'confirmation_depublication' => '¡Atención, un conteníu espublizáu apunta pa esti, y verase afectáu si lu desespublces!nn¿Seguro que quies camuda-y l’estau?', # MODIF
	'confirmation_publication' => '¡Atención, un conteníu al que apunta esti nun ta espublizáu!nn¿Seguro que quies camuda-y l’estau?', # MODIF
	'confirmation_suppression' => '¡Atención, un conteníu espublizáu apunta a esti, y verase afectáu si lu desanicies!nn¿Realmente quies desanicialu?', # MODIF

	// I
	'information_element_contenu' => '¡Atención, otru conteníu apunta a esti!', # MODIF

	// L
	'legende_liens_faux_objets' => 'Los enllaces en bermeyu y tachaos indiquen conteníos enllazaos que nun esisten.',

	// O
	'objets_avec_liens_depuis_courant' => 'Esti conteníu contién enllaces a estos:', # MODIF
	'objets_avec_liens_vers_courant' => 'Estos conteníos contienen enllaces a esti:', # MODIF

	// S
	'statut_poubelle' => 'Na basoria',
	'statut_prepa' => 'En preparación',
	'statut_prop' => 'Propuestu',
	'statut_publie' => 'Espublizáu',
	'statut_refuse' => 'Refugáu'
);
